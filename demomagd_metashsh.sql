-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 13, 2017 at 12:36 AM
-- Server version: 5.5.36-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `demomagd_metashsh`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `title_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `title`, `title_en`) VALUES
(6, 'd1b9de5hybrid.jpg', 'افهم يا ابراهيم', 'mohamed '),
(7, 'ec532a0Vodafone_logo.png', 'مخطط عبد الله', 'abduallh'),
(8, 'ed1b872satellite.jpg', 'الطريق الدائري الثاني', 'second road'),
(9, 'fa1cee25ix9lSzIPw_1476179426.jpg', 'الطريق الدائري الأول', 'first road'),
(10, 'a7f9cec_D8B5D988D8B1_D8B1D8A7D982D98AD987_D8ACD-1.jpg', 'تجريبى ١', 'test 1'),
(11, 'f40e613Vodafone_logo.png', 'الابيارابر', 'dddsds');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `gallery_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `image`, `gallery_id`) VALUES
(1, 'e40c246sebha.png', 1),
(3, 'ec0c93cearth213.png', 2),
(5, 'uploads/22b9caajpg', 6),
(6, 'uploads/57e0affjpg', 6),
(7, 'uploads/1573eb9jpeg', 10),
(8, 'uploads/5a04377jpeg', 10),
(9, 'uploads/c71b397jpeg', 10),
(10, 'uploads/1c0187ajpeg', 9),
(11, 'uploads/2280395jpeg', 9),
(12, 'uploads/68fb9f8jpeg', 9),
(13, 'uploads/0570939jpeg', 8),
(14, 'uploads/7ac76bajpeg', 8),
(15, 'uploads/400e1a8png', 8),
(16, 'uploads/af0daa2jpeg', 8),
(17, 'uploads/a1bececjpeg', 7);

-- --------------------------------------------------------

--
-- Table structure for table `goals`
--

CREATE TABLE IF NOT EXISTS `goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `description_en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `goals`
--

INSERT INTO `goals` (`id`, `image`, `description`, `description_en`) VALUES
(1, 'icon.jpg', '      123123123 \r\n\r\n\r\n123123123  ', '   dfxcvds');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `description_en` text NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `type` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `description`, `description_en`, `name_en`, `image`, `type`) VALUES
(11, 'ماجد مصطفي النحاس', ' رئيس قسم الجودة', '', '', '4b3b92femployee4.jpg', 1),
(12, 'أحمد عصام', 'رئيس مجلس الإدارة ', '', '', 'c2b3684employee5.jpg', 1),
(13, 'زكي محمود', 'عضو أول ', '', '', 'c8ee70aemployee6.jpeg', 2),
(14, 'سامح عبدالله ', '   عضو غير فعال ', 'member', ' Sameh abdallah', '4501bfaemployee7.jpeg', 2),
(15, 'أحمد إبراهيم  احمد  ', '    عضو رئيسي  فى العمل  ', 'Main member', 'Ahmed Ibrahem Ahmed', '21bc2eeemployee8.jpeg', 2),
(16, 'علي نبيل علي ', '  عضو دائم  ', 'Member', ' Ali nabil ali', 'd00b813employee2.jpg.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_01_094512_create_result_table', 1),
('2016_03_01_100428_create_question_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_01_094512_create_result_table', 1),
('2016_03_01_100428_create_question_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `image` longtext NOT NULL,
  `title_en` varchar(500) NOT NULL,
  `description_en` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `image`, `title_en`, `description_en`) VALUES
(23, 'الاتحاد الأوروبي يعاقب جوجل بسبب الاحتكار وخرق قواعد المنافسة', ' أعلنت مفوضية الاتحاد الأوروبي رسميا على لسان  مارجريت فيستجير أنها بصدد التفكير في معاقبة جوجل على إصرارها وضع عدد من التطبيقات الإفتراضية في هواتف أندرويد التي تنتجها مختلف الشركات ما يمنع على المنافسين من التفوق أو مجاراة المنافسة.\r\n\r\nوترى المفوضية أنه عندما يشتري المستخدم هاتف أندرويد ويجد أن جوجل كروم هو المتصفح الافتراضي فإنه غالبا ما يستخدمه بدل من تجربة العديد من المتصفحات قبل ان يحكم ويختار الأفضل بالنسبة له، ويحدث هذا في نظري أكثر مع المستخدمين الجدد والمبتدئين.\r\n\r\nوعادة ما تعمل المفوضية خلال هذا الشهر من كل سنة تحقيقات بشأن الشركات الناشطة في القارة العجوز والتحقق من أنشطتها التجارية والبحث عن أية سلوكيات تؤكد أنها تمارس الاحتكار من أجل معاقبتها وتتفق مختلف التقارير على أن جوجل فعلا سيتم تغريمها بمبلغ مالي كبير.\r\n\r\nوتقول مصادرنا أنه في حالة تأكد بالفعل أن هذا السلوك غير قانوني وأن جوجل تجبر الشركات العالمية المنتجة لهواتف أندرويد على تنصيب التطبيقات الافتراضية الخاصة بها فسيتم تغريم الشركة الأمريكية، وتضيف مصادرنا ان الغرامة ستكون نسبة من الايرادات التي حققت الشركة سنويا وبالعودة إلى المصدر يقول ان هذه النسبة هي 10 في المئة من الايرادات سيكون على جوجل دفعها للمفوضية.\r\n\r\nولأن جوجل حققت العام الماضي حوالي 74.5 مليار دولار فإنها ستكون مرغمة على دفع حوالي 7.45 مليون دولار.\r\n\r\nوبالتأكيد ستكون جوجل مرغمة على اعادة النظر في سياستها كي لا تتعرض مجددا لنفس العقوبات في هذا الوقت من العام القادم ومن جهات أخرى عالمية.\r\n\r\nولطالما أكد المحللين والمراقبين انزعاجهم من سياسة جوجل التي تفرض على الشركات المصنعة للهواتف الذكية بتنصيب عدد من تطبيقات جوجل بشكل افتراضي في هواتفها الذكية، ولأن عدد من تلك الشركات أيضا تطور عددا من تطبيقاتها فهي الأخرى تعمل على تنصيب المزيد منها واستهلاك مساحة تخزين أكبر مما هو متوقع.\r\n\r\nهذه السياسة تضر بالمساحة التخزينية في الهواتف الذكية، فمثلا إذا اشتريت نسخة 16 جيجا بايت من هاتف يعمل بنظام أندرويد يمكنك أن تجد أن 4 جيجا بايت منها شاغر بسبب النظام والواجهة والتطبيقات الافتراضية وهو ما يعد فعلا مزعج.\r\n\r\nومن ناحية أخرى هذا يسبب ضررا للشركات المنتجة لتطبيقات رائعة تتوفر على جوجل بلاي لكن لا تأتي بشكل مسبق مع هاتف أندرويد، فعوض جي ميل الذي يأتي مع أكثر من مليار هاتف ذكي نجد غياب تطبيق أوتلوك وياهو وهو ما يشكل ظلما للمنافسين وسلوكا يترجم احتكارية جوجل.', '08b5fd7download-compressor.jpg', 'xczc', 'sad'),
(24, 'أوراكل تطالب جوجل بـ 9.3 مليارات دولار في قضية جافا وأندرويد', ' رفعت شركة أوراكل حديثًا دعوى قضائية جديدة تطالب فيها جوجل بدفع ما يصل إلى 9.3 مليارات دولار من التعويضات، وذلك في القضية المعروفة بين الشركتين والمتعلقة بانتهاك حقوق استخدام لغة جافا في نظام التشغيل أندرويد.\r\n\r\nوتعود القضية التي تتهم فيها أوراكل المطورة للغة جافا نظيرتها جوجل صاحبة نظام التشغيل أندرويد بأن الأخير يستخدم أجزاء من لغة البرمجة على نحو غير قانوني، إلى نحو 5 سنوات خلت.\r\n\r\nوتقول شركة أوراكل المتخصصة في مجال قواعد البيانات إن جوجل استخدام هذه الأجزاء (مجموعة من الواجهات البرمجية APIs) يحتاج إلى الحصول على ترخيص منها، وهو ما لم تفعله جوجل.\r\n\r\nوكانت محكمة أميركية قد قضت في شهر أيار/مايو 2015 بأن جوجل لم تنتهك حقوق الملكية الخاصة بأوراكل، ورأت أن 97% من الواجهات البرمجية المستخدمة في أندرويد من تطوير جوجل نفسها، و 3% فقط هي من تطوير أوراكل لكنها غير قابلة لتسجيل حقوق النشر.\r\n\r\nويُنتظر أن تُعقد الجلسة الأولى في المعركة القضائية الجديدة يوم 9 أيار/مايو القادم، ويُتوقع أن يحضر الجلسات إيريك شميت الرئيس التنفيذي لشركة ألفابت (الشركة الأم لجوجل)، والذي تولى إدارة الفريق المطور للغة جافا في العام 1983 عندما كان يعمل في شركة Sun Microsystems المطور الأصلي للغة قبل أن تستحوذ عليها أوراكل.\r\n\r\nيُذكر أن جوجل بدأت اعتماد منصة OpenJDK الحرة ومفتوحة المصدر بدلًا من إصدار جافا القياسي Java SE  وذلك بدءًا من نسخة أندرويد التي أعلنت عنها قبل مدة  Android N.\r\n', '576ccffLarry_Ellison_CEO_of_Oracle_Corporation-598x337.jpg', '', '0'),
(26, ' الرئيسية / جوجل / جوجل قد تجعل لغة آبل سويفت لغة برمجة لأندرويد جوجل قد تجعل لغة آبل سويفت لغة برمجة لأندرويد ', ' \r\nالرئيسية / جوجل / جوجل قد تجعل لغة آبل سويفت لغة برمجة لأندرويد\r\nجوجل قد تجعل لغة آبل سويفت لغة برمجة لأندرويد\r\n\r\n \r\n\r\nذكر موقع “ذا نيكست ويب” The Next Web أن اجتماعاً عقد في العاصمة البريطانية لندن جمع ممثلين عن شركات فيس بوك وجوجل وأوبر Uber لمناقشة لغة البرمجة “سويفت” Swift التي أطلقتها واعتمدتها شركة “آبل” Apple لتطوير تطبيقات iOS.\r\n\r\nووفقاً لمصادر الموقع الخاصة، فإن شركة جوجل قد تفكر باعتماد لغة “سويفت” كلغة برمجة أساسية في تطوير تطبيقات أندرويد. في الوقت نفسه، كان كل من فيس بوك وأوبر يسعون إلى منح اهتمام أكبر للغة البرمجة ذاتها.\r\n\r\nيذكر أن جوجل تعتمد حالياً على “جافا” Java كلغة برمجة أساسية في تطوير تطبيقاتها.\r\n\r\nجوجل تريد أن تستغنى عن جافا وقد تتجه للغة آبل سويفت أو لغة آخرى تدعى Kotlin ولكن عملية تطوير الأخيرة بطيئة بعض الشئ لذلك قد لا تعتمد جوجل عليها . لكن كانت ظهرت تقارير منذ فترة أن جوجل لديها لغة برمجة خاصة بها تعمل عليها تنوي الإعتماد عليها بدلا من لغة جافا.\r\nعن لغة البرمجة “سويفت”\r\nانطلقت لغة البرمجة “سويفت” التي تم تطويرها من شركة “آبل” كبديل عن لغة “أوبجكتف سي” Objective C التي كانت اللغة المعتمدة في تطوير تطبيقات iOS، والتي تم الإعلان عنها خلال مؤتمر WWDC 2014، وفي وقت لاحق في نهاية العام 2015 تم إطلاقها كلغة برمجة مفتوحة المصدر، ما قد يشكل حافزاً لشركة جوجل لتبني هذه اللغة دون التأثير على بنية أندرويد مفتوحة المصدر.\r\n\r\nعلى الرغم من الصعوبات والتحديات التي قد تواجهها جوجل في تبني اللغة، إلا أن اعتمادها لن يكون مستحيلاً حيث يذكر أن مطوّر التطبيقات “روماين غوييت” Romain Goyet قام في وقت سابق بتجربة تطوير تطبيق يعمل على نظام التشغيل أندرويد باستخدام لغة “سويفت” ونجح في ذلك.', '028926eios8sdk.png', ' huhuhhh', '0kjijiih;vuyyrtr yuu'),
(37, 'تجريبى ١  ', 'تجريبى ١ تجريبى ١ تجريبى ١ تجريبى ١ تجريبى ١ تجريبى ١ تجريبى ١ ', '770bf01ka.jpg', 'test 1  ', ' test 1test 1test 1test 1test 1test 1test 1test 1test 1test 1'),
(38, 'شيبشيسبشيسب', ' يبشيسبشيبيسبشيب', 'ccc7ef9Chrysanthemum.jpg', 'dfadfffffffffffffffff', ' fffffffffffffffffffffffffffff');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('support@magdsoft.com', '26feb635c7a2ec55307cde050bd3f0f7acc06bcaa8c208cadb8f0ef5ee1c2fac', '2016-12-17 13:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `law` text COLLATE utf8_unicode_ci NOT NULL,
  `owner_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `law_en` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `owner`, `title`, `phone`, `law`, `owner_en`, `title_en`, `law_en`) VALUES
(13, 'English english', 'knkgnkg', 'knkgnkg', 'knkgnkg', '', '', ''),
(14, 'Arabic English', 'iifihehhf', 'iifihehhf', 'iifihehhf', '', '', ''),
(15, 'Arabic arabic', 'kjgihgi', 'kjgihgi', 'kjgihgi', '', '', ''),
(16, 'English Arabic', 'frrrrr', 'frrrrr', 'frrrrr', '', '', ''),
(17, 'غاالون', 'زلبتاةةوظ', 'زلبتاةةوظ', 'زلبتاةةوظ', '', '', ''),
(18, 'لهلنال', 'اىتزرة', 'اىتزرة', 'اىتزرة', '', '', ''),
(19, 'امىتةؤة', 'بازةؤي', 'بازةؤي', 'بازةؤي', '', '', ''),
(20, 'غعتبانن', 'اتالل', 'اتالل', 'اتالل', '', '', ''),
(21, 'تلبنو', 'اتابى', 'اتابى', 'اتابى', '', '', ''),
(22, 'جججج', 'ججج', 'ججج', 'ججج', '', '', ''),
(23, 'ججج', 'ججج', 'ججج', 'ججج', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `requests_gallery`
--

CREATE TABLE IF NOT EXISTS `requests_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  ` image` text NOT NULL,
  `request_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$MNWVOMrrQaWmrlIKnEdXF.m3MMjf8RGMj.CtMjiIHApnwQj7g.HqO', '36666', 'المدير', 'AYU3FMewMNbPlMmetVSC5r4PI7LZW6xAH1RTsdH9XqTAU9MIsbmsN89HLLbf', '2016-03-13 14:39:24', '2016-12-17 13:29:14'),
(7, 'امانى ', 'support@magdsoft.com', '123456', '01111741826', 'طالبة', NULL, '2016-03-14 13:59:58', '2016-03-14 13:59:58'),
(18, 'metashsh', 'm@m.com', '$2y$10$hfeTHY1y4dNbzXxXInmpmO29xwC9GdZdI1LAn82XLdHE5Z5K.nofS', '01271634925', 'طالب', NULL, '2016-03-14 16:38:55', '2016-03-14 16:38:55'),
(19, 'haya', 'alsosan@hotmail.com', '$2y$10$hfeTHY1y4dNbzXxXInmpmO29xwC9GdZdI1LAn82XLdHE5Z5K.nofS', '0506239150', 'زميلات', NULL, '2016-03-14 16:39:01', '2016-03-14 16:39:01'),
(20, 'mohamed', 'email@gmail.con', '12345', '011282826', 'طالبة', NULL, '2016-03-15 09:28:27', '2016-03-15 09:28:27'),
(21, 'qwert', 'qwert', 'qwerty', 'qwerty', 'ولي أمر', NULL, '2016-03-15 17:01:58', '2016-03-15 17:01:58'),
(22, 'تبنب', 'تبتيتت', 'جججج', 'ايتبت', 'ولي أمر', NULL, '2016-03-15 17:17:16', '2016-03-15 17:17:16'),
(23, 'عبود', 'abdullah-msr@outlook.com', '1234567890', '0549591054', 'طالبة', NULL, '2016-03-21 16:06:56', '2016-03-21 16:06:56'),
(24, 'غزيل', 'skystar--4@hotmail.com', 'amazing', '0556219070', 'زميلات', NULL, '2016-03-22 05:43:59', '2016-03-22 05:43:59'),
(25, 'غلو الدوسري ', 'rg1422h@gmail.com', '١٢٣٥', '٠٥٥٧٧٠٣٣٩١', 'زميلات', NULL, '2016-03-22 16:39:59', '2016-03-22 16:39:59'),
(26, 'فتون الدوسري', 'fn-fn3@hotmail.com', '0503416615', '0503419004', 'طالبة', NULL, '2016-03-25 10:40:26', '2016-03-25 10:40:26'),
(27, 'هيبه الخيال', 'miss_h.a.k@hotmail.com', '108666', '0553884058', 'زميلات', NULL, '2016-03-25 13:30:32', '2016-03-25 13:30:32'),
(28, 'lotfia', 'ltf79.eid@gmail.com', 'financika79', '07947846783', 'طالبة', NULL, '2016-03-25 16:05:11', '2016-03-25 16:05:11'),
(29, 'الحسين محمد قحل ', 'hmmg2007@hotmail.com', 'h0503410998', '0503410998', 'إدارة', NULL, '2016-03-26 16:25:15', '2016-03-26 16:25:15'),
(30, 'عبدالله محمود مشلح الدوسري', 'abom0104@.com', '1907', '0509950104', 'ولى أمر', NULL, '2016-03-26 16:43:21', '2016-03-26 16:43:21'),
(31, 'فرح ', 'fooofoo-1313@hotmail.com', '20402040', '', 'طالبة', NULL, '2016-03-26 17:01:29', '2016-03-26 17:01:29'),
(32, 'eslam', 'e@e.e', '123456', '01224343343', 'طالب', NULL, '2016-03-26 17:36:48', '2016-03-26 17:36:48'),
(33, 'ريم الوزره', 'the_waves_echo29@hotmail.com', 'reem123456', '0507479928', 'زميلات', NULL, '2016-03-27 14:34:58', '2016-03-27 14:34:58'),
(34, 'عبيد الجناح ', 'obaid36380@gamil.com', 'ob242424', '0554462424', 'ولى أمر', NULL, '2016-03-27 16:03:18', '2016-03-27 16:03:18'),
(35, 'عبدالله عبيد ال جناح ', 'al3ndleb2009@gmail.com', 'a1e4z6s2m9', '0540387019', 'ولى أمر', NULL, '2016-03-27 17:00:46', '2016-03-27 17:00:46'),
(36, 'مبارك الدوسري', 'asmr2009@hotmail.com', '0558896001', '0558896001', 'ولى أمر', NULL, '2016-03-27 17:14:40', '2016-03-27 17:14:40'),
(37, 'مشعل', 'modossari1@gmail.com', '19883450', '0597949433', 'ولى أمر', NULL, '2016-03-28 04:34:41', '2016-03-28 04:34:41'),
(38, 'منصور', 'mansuor1396@gmail.com', '4141738', '+966504800927', 'طالبة', NULL, '2016-03-29 16:00:22', '2016-03-29 16:00:22'),
(39, 'مبارك عبدالله الدوسري', 'mads10000@gmail.com', 'mads10000', '0504452395', 'ولى أمر', NULL, '2016-04-02 08:51:34', '2016-04-02 08:51:34');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
