<script type="text/javascript">
	try{ace.settings.loadState('main-container')}catch(e){}
</script>

<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<ul class="nav nav-list">
		<li>
			<a href="#">
				<i class="menu-icon fa fa-home"></i>
				<span class="menu-text">لوحة التحكم</span>
			</a>
			<b class="arrow"></b>
		</li>

		<li>
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-vcard"></i>
				<span class="menu-text">الإدارة</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li>
					<a href="{{ route('showManager') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-user"></i>&nbsp; المديرين
					</a>
					<b class="arrow"></b>
				</li>
				<li>
					<a href="{{ route('showMember') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-user-circle"></i>&nbsp; الموظفين
					</a>
					<b class="arrow"></b>
				</li>
					<li>
						<a href="{{ route('addManager') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							<i class="fa fa-plus"></i>  إضافة مدير
						</a>
						<b class="arrow"></b>
					</li>
					<li>
						<a href="{{ route('addMember') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							<i class="fa fa-plus"></i>&nbsp; إضافة موظف
						</a>
						<b class="arrow"></b>
					</li>
			</ul>
		</li>

		<li>
			<a href="#" class="dropdown-toggle">
				<i class="fa fa-cubes"></i>
				<span class="menu-text">الاهداف</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li>
					<a href="{{ route('showGoals') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-check-circle"></i>&nbsp; عرض الاهداف
					</a>
					<b class="arrow"></b>
				</li>
				<li>
					<a href="{{ route('addGoals') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-user"></i>&nbsp; اضافه هدف
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li>
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-vcard"></i>
				<span class="menu-text">الاخبار</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li>
					<a href="{{ route('showNews') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-eye"></i>&nbsp; عرض الاخبار
					</a>
					<b class="arrow"></b>
				</li>
				<li>
					<a href="{{ route('addNews') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						<i class="fa fa-plus"></i>&nbsp; اضافه خبر
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li>
			<a href="{{route('showGallery')}}">
				<i class="menu-icon fa fa-picture-o"></i>
				<span class="menu-text">المعرض</span>
			</a>
			<b class="arrow"></b>
		</li>

		<li>
			<a href="{{route('showRequests')}}">
				<i class="menu-icon fa fa-vcard-o"></i>
				<span class="menu-text">الطلبات</span>
			</a>
			<b class="arrow"></b>
		</li>

	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-right ace-save-state" data-icon1="ace-icon fa fa-angle-double-right" data-icon2="ace-icon fa fa-angle-double-left"></i>
	</div>
</div>
