<!DOCTYPE html>
<html lang="en">
    <head>
        <title>تسجيل الدخول</title>
        {!! Html::style('css/common/bootstrap.min.css') !!}
        {!! Html::style('css/common/font-awesome.min.css') !!}
        {!! Html::style('css/admin/fonts.googleapis.com.css') !!}
        {!! Html::style('css/admin/ace-rtl.min.css') !!}
        {!! Html::style('css/admin/ace.min.css') !!}
        {!! Html::style('css/admin/ace-skins.min.css') !!}
        {!! Html::style('css/admin/main.css') !!}
    </head>

    <body class="login-layout light-login rtl">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf blue"></i>
                                    <span class="blue">مجد</span><span class="white">سوفت</span>
                                </h1>
                                <h4 class="red" id="id-company-text">&copy; لوحة تحكم مطشش</h4>
                            </div>
                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee blue"></i>
                                                تسجيل دخول لوحة التحكم
                                            </h4>
                                            <div class="space-6"></div>
                                            <form class="form-horizontal" role="form" method="POST" action="{{url('/login')}}">
                                                {{ csrf_field() }}
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <input type="email" id="email" type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="البريد الإلكترونى" required autofocus />
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                            <input type="password" id="password" type="password" name="password" class="form-control" placeholder="كلمة المرور" required />
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </label>

                                                    <div class="space"></div>
                                                    <div class="clearfix">
                                                        <button type="submit" class="pull-right btn btn-sm btn-primary">
                                                                <i class="ace-icon fa fa-key"></i>
                                                                <span class="bigger-110">تسجيل الدخول</span>
                                                        </button>
                                                    </div>
                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>