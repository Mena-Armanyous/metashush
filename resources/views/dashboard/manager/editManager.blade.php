@extends('layouts.master')

@section('title')
    تعديل مدير
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>تعديل مدير</h2>
                <hr>
            </div>
            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="post" action="{{ route('updateMember', [$member->id]) }}" enctype="multipart/form-data">
                    {{csrf_field()}}


                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="name" class="control-label">الاسم</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="الاسم باللغه العربيه" required value="{{$member->name}}">
                            <input type="text" class="form-control" id="name_en" name="name_en" placeholder="Name with English language" required value="{{$member->name_en}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="name" class="control-label">التعريف</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" name="description" placeholder="التعريف باللغه العربيه" required value="{{$member->description}}">
                            <input type="text" class="form-control" id="description_en" name="description_en" placeholder="Description with English language" required value="{{$member->description_en}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">الصوره</label>
                        </div>
                        <div class="col-sm-10">
                            @if(!empty($member->image))
                                <img src="{{asset($member->image)}}" alt="Mountain View" style="width:304px;height:228px;">
                            @else
                                <p>لايوجد صوره</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">تعديل صوره</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">تعديل صوره</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">تعديل</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
