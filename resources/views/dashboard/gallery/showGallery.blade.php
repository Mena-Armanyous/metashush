@extends('layouts.master')

@section('title')
    المعرض
@endsection

@section('scripts')
    <style type="text/css">
    .rtl .ace-thumbnails>li>.tools {
    / left:0;
    text-align: center;
    right: 0px;
    }
    .ace-thumbnails>li>.tools.tools-bottom {
    width: auto;
    height: 42px;
    left: 0;
    right: 0;
    top: auto;
    bottom: -43px;
    }
    </style>
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>المعرض</h2>
                <hr>
            </div>
            <div class="col-log-12">
                <a class="btn btn-success" href="{{route('addGallery')}}"><i class="fa fa-plus">اضف معرض </i></a>
                <hr>
            </div>
            <div class="col-log-12">
                @if(count($gallery) > 0)
                    <div>
                        @foreach($gallery as $item)
                      <form action="{{route('deleteGallery', [$item->id])}}" method="POST">
                          {{csrf_field()}}
                        <ul class="ace-thumbnails">
                            <li style="width: 250px; height: 250px; display: inline-block;">
                                <a data-rel="colorbox" class="cboxElement">
                                    {{--@if(!$gallery->has('image'))--}}
                                    <img style="width: 250px; height: 250px;" src="{{asset($item->image)}}" alt="معرض">
                                    {{--@endif--}}
                                    <div class="text">
                                        <div class="inner">{{$item->title}}</div>
                                    </div>
                                </a>

                                <div class="tools tools-bottom">
                                    <button type="submit"  href="{{route('deleteGallery', [$item->id])}}" style="background-color:transparent;border:0px;">
                                        <i class="fa fa-trash fa-3x" style="color: red;"></i>
                                    </button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </form>
                            @endforeach
                    </div>
                    <div>
                        {{$gallery->links()}}
                    </div>
                @else
                    <p class="empty">لا يوجد معارض حاليا</p>
                @endif

            </div>
        </div>
    </div>


@endsection
