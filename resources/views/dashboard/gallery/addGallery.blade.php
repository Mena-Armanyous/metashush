@extends('layouts.master')

@section('title')
    اضافه معرض
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>اضافه معرض</h2>
                <hr>
            </div>
            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="POST" action="{{route('saveGallery')}}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="title" class="control-label">العنوان</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" placeholder="العنوان باللغه العربيه">
                            <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Title with English language">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">اضافه صوره رئيسيه</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="mainImage" name="mainImage" aria-describedby="fileHelp" >
                            <small id="fileHelp" class="form-text text-muted">اضف صوره رئيسيه</small>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group input_fields_wrap">
                        <div class="col-sm-2">
                            <label for="image">اضافه صوره ملحقه</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="image" name="subImage[]" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">اضف صوره ملحقه</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image"></label>
                        </div>
                        <div class="col-sm-10">
                            <button class="add_field_button btn-success">اضافه صوره ملحقه اخري</button>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">حفظ</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $('<div class="form-group input_fields_wrap">' +
                        '<div class="col-sm-2">' +
                        '<label for="image"></label>' +
                    '</div>' +
                    '<div class="col-sm-10">' +
                        '<input type="file" class="form-control-file" id="image" name="subImage[]" aria-describedby="fileHelp">' +
                        '<small id="fileHelp" class="form-text text-muted">اضف صوره ملحقه</small>' +
                        '<div><a href="#" class="remove_field">Remove</a></div>'+
                    '</div>' +
                    '</div>').insertAfter(wrapper); //add input box
                }
            });

            $('.form-horizontal').on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $('.input_fields_wrap').last().remove(); x--;
            })
        });
    </script>
@endsection


{{--<div class="form-group input_fields_wrap">--}}
    {{--<div class="col-sm-2">--}}
        {{--<label for="image">اضافه صوره ملحقه</label>--}}
    {{--</div>--}}
    {{--<div class="col-sm-10">--}}
        {{--<div><input type="text" name="subImage[]"></div>--}}
        {{--<small id="fileHelp" class="form-text text-muted">اضف صوره ملحقه</small>--}}
    {{--</div>--}}
    {{--<a href="#" class="remove_field">Remove</a>--}}
{{--</div>--}}