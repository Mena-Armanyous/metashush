@extends('layouts.master')

@section('title')
    تعديل هدف
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>تعديل عضو</h2>
                <hr>
            </div>
            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="POST" action="{{route('updateNews', [$news->id])}}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            <label class="control-label">عنوان الخبر</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" placeholder="عنوان الخبر باللغه العربيه" value="{{$news->title}}">
                            @if($errors->has('title'))

                                <span class="help-block">{{$errors->first('title')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('title_en') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            {{--<label class="control-label"> عنوان الخبر باللغه الانجليزيه</label>--}}
                        </div>
                        <div class="col-sm-10 has-errors">
                            <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Title with English language" value="{{$news->title_en}}">
                            @if($errors->has('title_en'))

                                <span class="help-block">{{$errors->first('title_en')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            <label class="control-label">تفاصيل الخبر</label>
                        </div>
                        <div class="col-sm-10">
                            <textarea class="form-control" value="{{old('')}}" rows="5" id="description" name="description" placeholder="تفاصيل الخبر باللغه العربيه">{{$news->description}}</textarea>
                            @if($errors->has('description'))
                                <span class="help-block">{{$errors->first('description')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description_en') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            {{--<label class="control-label">تفاصيل الخبر باللغه الانجليزيه</label>--}}
                        </div>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" id="description_en" name="description_en" placeholder="Description with English Language">{{$news->description_en}}</textarea>
                            @if($errors->has('description_en'))
                                <span class="help-block">{{$errors->first('description_en')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">الصوره</label>
                        </div>
                        <div class="col-sm-10">
                            @if(!empty($news->image))
                                <img src="{{asset($news->image)}}" alt="Mountain View" style="width:304px;height:228px;">
                            @else
                                <p>لايوجد صوره</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            <label for="image">تعديل صوره</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp" value="{{old('image')}}">
                            <small id="fileHelp" class="form-text text-muted">اضف صوره</small>
                            @if($errors->has('image'))
                                <span class="help-block">{{$errors->first('image')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">تعديل</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection




