@extends('layouts.master')

@section('title')
   الاخبار
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>    الاخبار</h2>
                <hr>
            </div>
            <div class="col-log-12">
                @if(count($news) > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>الاسم</th>
                            {{--<th>الوظيفه</th>--}}
                            <th>تعديل/حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($news as $item)
                            <tr>
                                <td>{{$item->title}}</td>
                                {{--<td>{{$item->description}}</td>--}}
                                <td>
                                    <div class="hidden-sm hidden-xs btn-group">

                                        <a class="btn btn-xs btn-success"  href="{{ route('editNews', [$item->id]) }}">
                                            <i class="ace-icon fa fa-pencil bigger-120"> تعديل </i>
                                        </a>

                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal{{$item->id}}">
                                            <i class="fa fa-trash" aria-hidden="true"> حذف </i>
                                        </a>


                                    </div>

                                </td>

                            </tr>

                            <!-- Modal -->
                            <div class="modal fade bs-modal-sm" id="myModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">

                                        <div class="modal-body">
                                            <div id="myTabContent" class="tab-content">
                                                <div class="tab-pane fade active in" id="signin">
                                                    <form class="form-horizontal" method="POST" action="{{route('deleteNews',[$item->id])}}">
                                                        {{csrf_field()}}
                                                        <fieldset>

                                                            <!-- Button -->
                                                            <div class="control-group">
                                                                <label class="control-label" for="search"></label>
                                                                <div class="controls"> هل انت تريد حذف الخبر "{{$item->title}}"؟</div>
                                                            </div>

                                                            <!-- Button -->
                                                            <div class="control-group">
                                                                <label class="control-label" for="signin"></label>
                                                                <center>
                                                                    <div class="controls">
                                                                        <button type="submit" class="btn btn-danger">حذف</button>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">اغلاق</button>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <!-- End Modal-->
                        @endforeach
                        </tbody>

                    </table>
                    <div>
                        {{$news->links()}}
                    </div>

                @else
                    <p class="empty">لا يوجد اخبار حاليا</p>
                @endif

            </div>
        </div>
    </div>


@endsection




