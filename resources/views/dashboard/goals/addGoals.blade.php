@extends('layouts.master')

@section('title')
  اضافه هدف
@endsection
@section('content')


<div class="section">
      <div class="container">
        <div class="col-log-12">
          <h2>اضافه هدف</h2>
          <hr>
        </div>
          <div class="col-md-8">
            <form class="form-horizontal" role="form" method="POST" action="{{route('saveGoals')}}" enctype="multipart/form-data">
              {{csrf_field()}}


              <div class="form-group">
                <div class="col-sm-2">
                  <label for="name" class="control-label">الهدف</label>
                </div>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="description" name="description" placeholder="الهدف باللغه العربيه" required>
                  <input type="text" class="form-control" id="description_en" name="description_en" placeholder="الهدف باللغه الانجليزيه" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-2">
                   <label for="image">اضافه صوره</label>
                 </div>
                 <div class="col-sm-10">
                   <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp" required="">
                   <small id="fileHelp" class="form-text text-muted">اضف صوره</small>
                 </div>
              </div>

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default">حفظ</button>
                </div>
              </div>
            </form>
          </div>
      </div>
    </div>


@endsection
