@extends('layouts.master')

@section('title')
    تعديل مدير
@endsection
@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>تعديل مدير</h2>
                <hr>
            </div>
            <div class="col-md-8">
                <form class="form-horizontal" role="form" method="post" action="{{ route('updateGoals', [$goals->id]) }}" enctype="multipart/form-data">
                    {{csrf_field()}}


                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            <label for="name" class="control-label">التعريف</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" name="description" placeholder="التعريف باللغه العربيه" value="{{$goals->description}}">
                            @if($errors->has('description'))

                                <span class="help-block">{{$errors->first('description')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description_en') ? ' has-error' : '' }}">
                        <div class="col-sm-2">
                            <label for="name" class="control-label"></label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description_en" name="description_en" placeholder="Description with English language" value="{{$goals->description_en}}">
                            @if($errors->has('description_en'))

                                <span class="help-block">{{$errors->first('description_en')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">الصوره</label>
                        </div>
                        <div class="col-sm-10">
                            @if(!empty($goals->image))
                                <img src="{{asset($goals->image)}}" alt="Mountain View" style="width:304px;height:228px;">
                            @else
                                <p>لايوجد صوره</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2">
                            <label for="image">تعديل صوره</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">تعديل صوره</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">تعديل</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
