@extends('layouts.master')

@section('title')
    الطلبات
@endsection

@section('extrascripts')
    <style type="text/css">
        fieldset.for-panel {
            background-color: #fcfcfc;
            border: 1px solid #999;
            border-radius: 4px;
            padding:15px 10px;
            background-color: #d9edf7;
            border-color: #bce8f1;
            background-color: #f9fdfd;
            margin-bottom:12px;
        }
        fieldset.for-panel legend {
            background-color: #fafafa;
            border: 1px solid #ddd;
            border-radius: 5px;
            color: #4381ba;
            font-size: 14px;
            font-weight: bold;
            line-height: 10px;
            margin: inherit;
            padding: 7px;
            width: auto;
            background-color: #d9edf7;
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')


    <div class="section">
        <div class="container">
            <div class="col-log-12">
                <h2>الطلبات</h2>
                <hr>
            </div>
            <div class="col-log-12">
                @if(count($requests) > 0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>صاحب العقار</th>
                            <th>رقم الهاتف</th>
                            <th>عرض/حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($requests as $item)
                            <tr>
                                <td>{{$item->owner}}</td>
                                <td>{{$item->phone}}</td>
                                <td>
                                    <div class="hidden-sm hidden-xs btn-group">

                                        <a class="btn btn-xs btn-primary"  data-toggle="modal" data-target="#showModal{{$item->id}}">
                                            <i class="ace-icon fa fa-eye bigger-120"> عرض </i>
                                        </a>

                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#removeModal{{$item->id}}">
                                            <i class="fa fa-trash" aria-hidden="true"> حذف </i>
                                        </a>


                                    </div>

                                </td>

                            </tr>

                            <!-- Remove Request Modal -->
                            <div class="modal fade bs-modal-sm" id="removeModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">

                                        <div class="modal-body">
                                            <div id="myTabContent" class="tab-content">
                                                <div class="tab-pane fade active in" id="signin">
                                                    <form class="form-horizontal" method="POST" action="{{route('deleteRequests',[$item->id])}}">
                                                        {{csrf_field()}}
                                                        <fieldset>

                                                            <!-- Button -->
                                                            <div class="control-group">
                                                                <label class="control-label" for="search"></label>
                                                                <div class="controls"> هل انت تريد حذف طلب "{{$item->owner}}"؟</div>
                                                            </div>

                                                            <!-- Button -->
                                                            <div class="control-group">
                                                                <label class="control-label" for="signin"></label>
                                                                <center>
                                                                    <div class="controls">
                                                                        <button type="submit" class="btn btn-danger">حذف</button>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">اغلاق</button>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <!-- End Modal-->

                                    <!-- Show Request Modal -->

                            <div class="modal fade bs-modal-sm" id="showModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" >
                                <div class="modal-dialog modal-sm" style="width: 50%">
                                    <div class="modal-content">

                                    <div class="modal-body" >
                                        <div id="myTabContent" class="tab-content">
                                            <div class="tab-pane fade active in" id="signin">
                                                <form class="form-horizontal" method="POST" action="#">
                                                    {{csrf_field()}}
                                                    <fieldset class="for-panel">
                                                        <legend>التفاصيل</legend>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-horizontal">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-horizontal">
                                                                                <label class="col-xs-5 control-label">صاحب العقار :</label>
                                                                                <p class="form-control-static">{{$item->owner}}</p>
                                                                                <label class="col-xs-5 control-label">عنوان العقار :</label>
                                                                                <p class="form-control-static">{{$item->title}}</p>
                                                                                <label class="col-xs-5 control-label">رقم الهاتف : </label>
                                                                                <p class="form-control-static">{{$item->phone}}</p>
                                                                                <label class="col-xs-5 control-label">المصدر القانوني : </label>
                                                                                <p class="form-control-static">{{$item->law}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>


                                        </div>

                                      </div>
                                    </div>
                                </div>
                            </div>

                                            <!-- End Modal-->
                        @endforeach

                        </tbody>
                    </table>
                    <div>
                        {{$requests->links()}}
                    </div>

                @else
                    <p class="empty">لا يوجد طلبات حاليا</p>
                @endif

            </div>
        </div>
    </div>


@endsection




