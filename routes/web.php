<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('bcrypt', function ()
{
    return bcrypt(123456);
});
Route::get('/', 'DashboardController@login')->name('login');
Route::get('/logout', 'DashboardController@logout')->name('logout');
##################################
//Home Route
Route::get('home', function()
{
    return view('home');
});

Route::group(['middleware' => 'auth'], function () {
        //Goals Routes
        #add
        Route::get('addGoals', 'DashboardController@addGoals')->name('addGoals');
        Route::post('saveGoals', 'DashboardController@saveGoals')->name('saveGoals');
        #show
        Route::get('showGoals', 'DashboardController@showGoals')->name('showGoals');
        #edit
        Route::get('editGoals/{id}', 'DashboardController@editGoals')->name('editGoals');
        Route::post('updateGoals/{id}', 'DashboardController@updateGoals')->name('updateGoals');
        #delete
        Route::post('deleteGoals{id}', 'DashboardController@deleteGoals')->name('deleteGoals');
        ##################################

        //Memebers Routes
        #add
        Route::get('addMember', 'DashboardController@addMember')->name('addMember');
        Route::post('saveMember', 'DashboardController@saveMember')->name('saveMember');
        #show
        Route::get('showMember', 'DashboardController@showMember')->name('showMember');
        #edit
        Route::get('editMember/{id}', 'DashboardController@editMember')->name('editMember');
        Route::post('updateMember/{id}', 'DashboardController@updateMember')->name('updateMember');
        #delete
        Route::post('deleteMember{id}', 'DashboardController@deleteMember')->name('deleteMember');
        ##################################

        //Managers Routes
        #show
        Route::get('showManager', 'DashboardController@showManager')->name('showManager');
        #add
        Route::get('addManager', 'DashboardController@addManager')->name('addManager');
        Route::post('saveManager', 'DashboardController@saveManager')->name('saveManager');
        #edit
        Route::get('editManager/{id}', 'DashboardController@editManager')->name('editManager');
        Route::post('updateManager/{id}', 'DashboardController@updateManager')->name('updateManager');
        #delete
        Route::post('deleteManager{id}', 'DashboardController@deleteManager')->name('deleteManager');
        ##################################

        //News Routes
        #show
        Route::get('showNews', 'DashboardController@showNews')->name('showNews');
        #add
        Route::get('addNews', 'DashboardController@addNews')->name('addNews');
        Route::post('saveNews', 'DashboardController@saveNews')->name('saveNews');
        #edit
        Route::get('editNews/{id}', 'DashboardController@editNews')->name('editNews');
        Route::post('updateNews/{id}', 'DashboardController@updateNews')->name('updateNews');
        #delete
        Route::post('deleteNews{id}', 'DashboardController@deleteNews')->name('deleteNews');
        ##################################

        //Requests Routes
        #show
        Route::get('showRequests', 'DashboardController@showRequests')->name('showRequests');
        #delete
        Route::post('deleteRequests{id}', 'DashboardController@deleteRequests')->name('deleteRequests');

        ##################################

        //Gallery Routes
        #show
        Route::get('showGallery', 'DashboardController@showGallery')->name('showGallery');
        #add
        Route::get('addGallery', 'DashboardController@addGallery')->name('addGallery');
        Route::post('saveGallery', 'DashboardController@saveGallery')->name('saveGallery');
        #Delete
        Route::post('deleteGallery{id}', 'DashboardController@deleteGallery')->name('deleteGallery');


});




