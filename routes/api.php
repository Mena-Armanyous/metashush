<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array('domain' => 'metashash.magdsoft.com', 'prefix' => 'metashash'), function() {

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
});

Route::post('getGallries','API\APIController@getGallries');
Route::post('getGalleryImages/{galleryid}','API\APIController@getGalleryImages');
Route::post('getManagers','API\APIController@getManagers');
Route::post('getMembers','API\APIController@getMembers');
Route::post('getNews','API\APIController@getNews');
Route::post('sendRequest','API\APIController@sendRequest');