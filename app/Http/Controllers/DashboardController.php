<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Gallery_Image;
use App\News;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use MercurySeries\Flashy\Flashy;
use Validator;
use App\Goals;
use App\Member;
use App\Requests;
use Auth;


class DashboardController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    //Goals Controller

    public function showGoals()
    {
        $goals = Goals::paginate(30);
        return view('dashboard.goals.showGoals', compact('goals'));
    }

    public function addGoals()
    {
        return view('dashboard.goals.addGoals');
    }

    public function saveGoals(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'description' => 'required|max:255',
            'description_en' => 'required|max:255',
            'image' => 'required'
        ]);

        if($validate->fails())
        {
            return redirect()->back()->with($validate->errors());
        }
        $goals = new Goals();
        $goals->description = $request->description;
        $goals->description_en = $request->description_en;

        if(!empty($request->image))
        {

            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $goals->image = $path . '/' . $filename;
        }

        $goals->save();

        Flashy::success('تم اضافه الهدف بنجاح');
        return redirect()->back();

    }

    public function editGoals($id)
    {
        $goals = Goals::find($id);
        return view('dashboard.goals.editGoals', compact('goals'));
    }

    public function updateGoals(Request $request, $id)
    {

        $rules = [
            'description' => 'required',
        ];
        $msg = [
            'description.required' => 'يحب ادخال الهدف',
        ];
        $errors = Validator::make($request->all(), $rules, $msg);
        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $goals = Goals::find($id);
        $goals->description = $request->description;
        $goals->description_en = $request->description_en;

        if(!empty($request->image))
        {

            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $goals->image = $path . '/' . $filename;
        }

        $goals->save();

        Flashy::success('تم تعديل الهدف بنجاح');

        return redirect()->route('showGoals');

    }


    //Gallery Controller

    public function showGallery()
    {
        $gallery = Gallery::paginate(30);
        return view('dashboard.gallery.showGallery',compact('gallery'));
    }

    public function addGallery()
    {
        return view('dashboard.gallery.addGallery');
    }

    public function saveGallery(Request $request)
    {
        $rule = [
            'mainImage' => 'required',
            'title' => 'required',
        ];
        $msg = [
            'subImage.required' => 'يجب ادخال صوره رئيسيه للمعرض',
            'title.required' => 'يجب ادخال صوره عنوان للمعرض',
        ];

        $errors = Validator::make($request->all(),$rule,$msg);
        if($errors->fails())
        {
            dd($errors);
            return redirect()->back()->withErrors($errors);
        }

        $gallery = new Gallery();
        $gallery->title = $request->title;
        $gallery->title_en = $request->title_en;
        if(!empty($request->mainImage))
        {

            $file = $request->mainImage;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $gallery->image = $path . '/' . $filename;
        }
        $gallery->save();

        if(!empty($request->subImage)) {

            foreach ($request->subImage as $item) {

                $file = $item;
                $filename = str_random(6) . time() . $file->getClientOriginalName();
                $path = 'MtashashImages';
                $file->move($path,$filename);
                $item = $path . '/' . $filename;

                $subimage = new Gallery_Image();
                $subimage->image = $item;
                $subimage->gallery_id = $gallery->id;
                $subimage->save();

            }
        }



        dd('ok');
       Flashy::success('تم اضافه المعرض بنجاح');



        return redirect()->route('showGallery');





    }

    public function deleteGallery(Gallery $id)
    {
        $id->delete();

        Flashy::success('تم الحذف بنجاح');
    }

    //Member Contoller

    public function addMember()
    {
        return view('dashboard.member.addMember');
    }

    public function saveMember(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'name_en' => 'required|max:255',
            'description' => 'required|max:255',
            'description_en' => 'required|max:255',
            'image' => 'required'
        ]);

        if($validate->fails())
        {
            return redirect()->back()->with($validate->errors());
        }

        $manager = new Member();
        $manager->name = $request->name;
        $manager->name_en = $request->name_en;
        $manager->description = $request->description;
        $manager->description_en = $request->description_en;
        $manager->type = 2;

        if(!empty($request->image))
        {

            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $manager->image = $path . '/' . $filename;
        }
        $manager->save();

        Flashy::success('تم اضافه المدير بنجاح');

        return redirect()->back();

    }

    public function showMember()
    {
        $member = Member::where('type', 2)->paginate(30);

        return view('dashboard.member.showMember', compact('member'));
    }

    public function editMember($id)
    {
        $member = Member::find($id);
        return view('dashboard.member.editMember', compact('member'));
    }

    public function deleteMember(Member $id)
    {
        $id->delete();
        Flashy::success('تم حذف العضو بنجاح');
        return redirect()->route('showMember');
    }

    //Manager Controller

    public function addManager()
    {
        return view('dashboard.member.addMember');
    }

    public function saveManager(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'name_en' => 'required|max:255',
            'description' => 'required|max:255',
            'description_en' => 'required|max:255',
            'image' => 'required'
        ]);

        if($validate->fails())
        {
            return redirect()->back()->with($validate->errors());
        }

        $manager = new Member();
        $manager->name = $request->name;
        $manager->name_en = $request->name_en;
        $manager->description = $request->description;
        $manager->description_en = $request->description_en;
        $manager->type = 1;

        if(!empty($request->image))
        {

            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $manager->image = $path . '/' . $filename;
        }
        $manager->save();

        Flashy::success('تم اضافه المدير بنجاح');

        return redirect()->back();

    }

    public function showManager()
    {
        $member = Member::where('type', 1)->paginate(30);

        return view('dashboard.member.showMember', compact('member'));
    }

    public function editManager($id)
    {
        $member = Member::find($id);
        return view('dashboard.member.editMember', compact('member'));
    }

    public function updateManager(Request $request, $id)
    {

        $validate = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'name_en' => 'required|max:255',
            'description' => 'required|max:255',
            'description_en' => 'required|max:255',
        ]);

        if($validate->fails())
        {
            return redirect()->back()->with($validate->errors());
        }

        $manager = Member::find($id);
        $manager->name = $request->name;
        $manager->name_en = $request->name_en;
        $manager->description = $request->description;
        $manager->description_en = $request->description_en;
        $manager->type = 2;

        if(!empty($request->image))
        {

            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path,$filename);
            $manager->image = $path . '/' . $filename;
        }
        $manager->save();

        Flashy::success('تم تعديل الموظف بنجاح');

        return redirect()->back();
    }

    //News Controller

    public function showNews()
    {
        $news = News::paginate(30);
        return view('dashboard.news.showNews', compact('news'));
    }

    public function addNews()
    {
        return view('dashboard.news.addNews');
    }

    public function saveNews(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'image' => 'required',
        ];
        $msg = [
            'title.required' => 'يجب ادخال عنوان الخبر',
            'description.required' => 'يحب ادخال تفاصيل الخبر',
            'image.required' => 'يجب ادخال صوره',
        ];
        $errors = Validator::make($request->all(), $rules, $msg);
        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $news = new News();
        $news->title = $request->title;
        $news->title_en = $request->title_en;
        $news->description = $request->description;
        $news->description_en = $request->description_en;
        if(!empty($request->image))
        {
            $file = $request->image;
            $filename = str_random(6) .time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path.$filename);
            $news->image = $path . '/' . $filename;
        }
//        dd($request);

        $news->save();

        Flashy::success('تم اضافه الخبر بنجاح');

        return redirect()->back();

    }

    public  function editNews($id)
    {
        $news = News::find($id);

        return view('dashboard.news.editNews', compact('news'));
    }

    public  function updateNews(Request $request, $id)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'image' => 'required',
        ];
        $msg = [
            'title.required' => 'يجب ادخال عنوان الخبر',
            'description.required' => 'يحب ادخال تفاصيل الخبر',
            'image.required' => 'يجب ادخال صوره',
        ];
        $errors = Validator::make($request->all(), $rules, $msg);
        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }
        $news = News::find($id);
        $news->title = $request->title;
        $news->title_en = $request->title_en;
        $news->description = $request->description;
        $news->description_en = $request->description_en;
        if(!empty($request->image))
        {
            $file = $request->image;
            $filename = str_random(6) .time() . $file->getClientOriginalName();
            $path = 'MtashashImages';
            $file->move($path.$filename);
            $news->image = $path . '/' . $filename;
        }
//        dd($request);

        $news->save();
        Flashy::success('تم تعديل الخبر بنجاح');

        return redirect()->route('showNews');
    }

    public function deleteNews(News $id)
    {
        $id->delete();
        Flashy::success('تم حذف الخبر بنجاح');
        return redirect()->route('showNews');
    }

    //Requests Controller

    public function showRequests()
    {

        $requests = Requests::paginate(30);
        return view('dashboard.requests.showRequests', compact('requests'));
    }

    public function deleteRequests(Requests $id)
    {
        $id->delete();
        Flashy::success('تم حذف الطلب بنجاح');
        return redirect()->route('showRequests');
    }






}
