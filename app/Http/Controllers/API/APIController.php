<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Requests;
use App\requests_gallery;

class APIController extends Controller
{
    //This function will be used to get the galleries info from the gallery table
    public function getGallries(){
        
        $galleries = DB::table('gallery')->select('id','title','title_en','image')->get();
        return response()->json($galleries);
    }

    //This function will be used to get the images of the gallery from the gallery_images table
    public function getGalleryImages($request){
        $galleryID=$request;
        $galleries = DB::table('gallery_images')->select('image')->where('gallery_id',$galleryID)->get();
        return response()->json($galleries);
    }

    //This function will be used to get the managers from the members table
    public function getManagers(){
        $managers = DB::table('members')->select('name','name_en','description AS desc','description_en AS desc_en','image')->where('type','=',1)->get();
        return response()->json($managers);
    }

    //This function will be used to get the managers from the members table
    public function getMembers(){
        $members = DB::table('members')->select('name','name_en','description AS desc','description_en AS desc_en','image')->where('type','=',2)->get();
        return response()->json($members);
    }

    //This function will be used to get the managers from the news table
    public function getNews(){
        $news = DB::table('news')->select('title','title_en','description AS desc','description_en AS desc_en','image')->get();
        return response()->json($news);
    }

    //
    public function sendRequest(Request $req){

        //save the vlues to table Requests
        $modRequests = new Requests;
        $modRequests->owner = $req->owner;
        $modRequests->title = $req->title;
        $modRequests->phone = $req->phone;
        $modRequests->law = $req->law;
        $modRequests->save();
        
        //save the value to table requests_gallery
        if ($req->hasFile('uploadedfile')) {
            $files = $req->file('uploadedfile');
            $modReqGal = new requests_gallery;
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $picture = $filename;
                $destinationPath = base_path() . '\public\MtashashImages\requests_gallery';
                $file->move($destinationPath, $picture);
                $modReqGal->image = $picture;
                $modReqGal->request_id = $modRequests->id;
                $modReqGal->save();
            }
        }
    }
}