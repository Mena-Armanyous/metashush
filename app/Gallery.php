<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    public function Gallery_Image()
    {
        return $this->hasMany('App\Gallery_Image', 'gallery_id');
    }
}
