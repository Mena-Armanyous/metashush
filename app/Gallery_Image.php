<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_Image extends Model
{
    protected $table = 'gallery_images';

    public function Gallery()
    {
        return $this->belongsTo('App\Gallery', 'gallery_id');
    }
}
