<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    protected $table = 'requests';
    protected $attributes = [
        'owner_en' => '',
        'title_en' => '',
        'law_en' => ''
    ];
    public $timestamps = false;
}
