<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requests_gallery extends Model
{
    protected $table = 'requests_gallery';
    public $timestamps = false;

}
